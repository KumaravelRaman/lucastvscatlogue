
package Model.AppPartlist;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AppPart {

    @SerializedName("data")
    private List<AppPArtDetails> mData;
    @SerializedName("result")
    private String mResult;

    public List<AppPArtDetails> getData() {
        return mData;
    }

    public void setData(List<AppPArtDetails> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
