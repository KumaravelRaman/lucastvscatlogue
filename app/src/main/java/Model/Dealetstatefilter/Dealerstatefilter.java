
package Model.Dealetstatefilter;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Dealerstatefilter {

    @SerializedName("data")
    private List<Dealerstatedetails> mData;
    @SerializedName("result")
    private String mResult;

    public List<Dealerstatedetails> getData() {
        return mData;
    }

    public void setData(List<Dealerstatedetails> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
