package Model.Wheretouse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetChildDetails{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private List<FullUnitItem> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<FullUnitItem> data){
		this.data = data;
	}

	public List<FullUnitItem> getData(){
		return data;
	}
}