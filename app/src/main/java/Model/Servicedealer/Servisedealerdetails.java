
package Model.Servicedealer;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Servisedealerdetails {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Address1")
    private String mAddress1;
    @SerializedName("Allocation")
    private String mAllocation;
    @SerializedName("AuditorName_admin")
    private String mAuditorNameAdmin;
    @SerializedName("AuditorName_service")
    private String mAuditorNameService;
    @SerializedName("AuditorName_tool")
    private String mAuditorNameTool;
    @SerializedName("AuditorName_warrenty")
    private String mAuditorNameWarrenty;
    @SerializedName("AuditorName_work")
    private String mAuditorNameWork;
    @SerializedName("Category")
    private String mCategory;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Contactperson")
    private String mContactperson;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("Dealerid")
    private String mDealerid;
    @SerializedName("Dealername")
    private String mDealername;
    @SerializedName("Depocode")
    private String mDepocode;
    @SerializedName("Depotname")
    private String mDepotname;
    @SerializedName("Email_id")
    private String mEmailId;
    @SerializedName("id")
    private String mId;
    @SerializedName("Landlineno")
    private String mLandlineno;
    @SerializedName("Mobileno")
    private String mMobileno;
    @SerializedName("Pincode")
    private String mPincode;
    @SerializedName("Region")
    private String mRegion;
    @SerializedName("Reportingtvsgroup")
    private String mReportingtvsgroup;
    @SerializedName("sno")
    private String mSno;
    @SerializedName("State")
    private String mState;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("TotalAvg")
    private String mTotalAvg;
    @SerializedName("TotalRating")
    private String mTotalRating;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String Address) {
        mAddress = Address;
    }

    public String getAddress1() {
        return mAddress1;
    }

    public void setAddress1(String Address1) {
        mAddress1 = Address1;
    }

    public String getAllocation() {
        return mAllocation;
    }

    public void setAllocation(String Allocation) {
        mAllocation = Allocation;
    }

    public String getAuditorNameAdmin() {
        return mAuditorNameAdmin;
    }

    public void setAuditorNameAdmin(String AuditorNameAdmin) {
        mAuditorNameAdmin = AuditorNameAdmin;
    }

    public String getAuditorNameService() {
        return mAuditorNameService;
    }

    public void setAuditorNameService(String AuditorNameService) {
        mAuditorNameService = AuditorNameService;
    }

    public String getAuditorNameTool() {
        return mAuditorNameTool;
    }

    public void setAuditorNameTool(String AuditorNameTool) {
        mAuditorNameTool = AuditorNameTool;
    }

    public String getAuditorNameWarrenty() {
        return mAuditorNameWarrenty;
    }

    public void setAuditorNameWarrenty(String AuditorNameWarrenty) {
        mAuditorNameWarrenty = AuditorNameWarrenty;
    }

    public String getAuditorNameWork() {
        return mAuditorNameWork;
    }

    public void setAuditorNameWork(String AuditorNameWork) {
        mAuditorNameWork = AuditorNameWork;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String Category) {
        mCategory = Category;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String City) {
        mCity = City;
    }

    public String getContactperson() {
        return mContactperson;
    }

    public void setContactperson(String Contactperson) {
        mContactperson = Contactperson;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String Date) {
        mDate = Date;
    }

    public String getDealerid() {
        return mDealerid;
    }

    public void setDealerid(String Dealerid) {
        mDealerid = Dealerid;
    }

    public String getDealername() {
        return mDealername;
    }

    public void setDealername(String Dealername) {
        mDealername = Dealername;
    }

    public String getDepocode() {
        return mDepocode;
    }

    public void setDepocode(String Depocode) {
        mDepocode = Depocode;
    }

    public String getDepotname() {
        return mDepotname;
    }

    public void setDepotname(String Depotname) {
        mDepotname = Depotname;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String EmailId) {
        mEmailId = EmailId;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getLandlineno() {
        return mLandlineno;
    }

    public void setLandlineno(String Landlineno) {
        mLandlineno = Landlineno;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String Mobileno) {
        mMobileno = Mobileno;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String Pincode) {
        mPincode = Pincode;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String Region) {
        mRegion = Region;
    }

    public String getReportingtvsgroup() {
        return mReportingtvsgroup;
    }

    public void setReportingtvsgroup(String Reportingtvsgroup) {
        mReportingtvsgroup = Reportingtvsgroup;
    }

    public String getSno() {
        return mSno;
    }

    public void setSno(String sno) {
        mSno = sno;
    }

    public String getState() {
        return mState;
    }

    public void setState(String State) {
        mState = State;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String Status) {
        mStatus = Status;
    }

    public String getTotalAvg() {
        return mTotalAvg;
    }

    public void setTotalAvg(String TotalAvg) {
        mTotalAvg = TotalAvg;
    }

    public String getTotalRating() {
        return mTotalRating;
    }

    public void setTotalRating(String TotalRating) {
        mTotalRating = TotalRating;
    }

}
