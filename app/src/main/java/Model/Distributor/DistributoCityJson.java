package Model.Distributor;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DistributoCityJson{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private List<String> data;

	public String getResult(){
		return result;
	}

	public List<String> getData(){
		return data;
	}
}