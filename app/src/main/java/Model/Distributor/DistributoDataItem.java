package Model.Distributor;

import com.google.gson.annotations.SerializedName;

public class DistributoDataItem {

	@SerializedName("Mobileno")
	private String mobileno;

	@SerializedName("Company")
	private String company;

	@SerializedName("Landlineno")
	private String landlineno;

	@SerializedName("address")
	private String address;

	@SerializedName("Contactperson")
	private String contactperson;

	@SerializedName("city")
	private String city;

	@SerializedName("Email_id")
	private String emailId;

	@SerializedName("customercode")
	private String customercode;

	@SerializedName("state")
	private String state;

	@SerializedName("region")
	private String region;

	public String getMobileno(){
		return mobileno;
	}

	public String getCompany(){
		return company;
	}

	public String getLandlineno(){
		return landlineno;
	}

	public String getAddress(){
		return address;
	}

	public String getContactperson(){
		return contactperson;
	}

	public String getCity(){
		return city;
	}

	public String getEmailId(){
		return emailId;
	}

	public String getCustomercode(){
		return customercode;
	}

	public String getState(){
		return state;
	}

	public String getRegion(){
		return region;
	}
}