package Model.Distributor;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DistributorData{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private List<DistributoDataItem> data;

	public String getResult(){
		return result;
	}

	public List<DistributoDataItem> getData(){
		return data;
	}
}