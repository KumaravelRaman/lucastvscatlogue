
package Model.Segments;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Segmentlist {

    @SerializedName("Segment_image")
    private String mSegmentImage;
    @SerializedName("SegmentName")
    private String mSegmentName;
    @SerializedName("Segment_priority")
    private Long mSegmentPriority;
    @SerializedName("Segmentid")
    private Long mSegmentid;

    public String getSegmentImage() {
        return mSegmentImage;
    }

    public void setSegmentImage(String SegmentImage) {
        mSegmentImage = SegmentImage;
    }

    public String getSegmentName() {
        return mSegmentName;
    }

    public void setSegmentName(String SegmentName) {
        mSegmentName = SegmentName;
    }

    public Long getSegmentPriority() {
        return mSegmentPriority;
    }

    public void setSegmentPriority(Long SegmentPriority) {
        mSegmentPriority = SegmentPriority;
    }

    public Long getSegmentid() {
        return mSegmentid;
    }

    public void setSegmentid(Long Segmentid) {
        mSegmentid = Segmentid;
    }

}
