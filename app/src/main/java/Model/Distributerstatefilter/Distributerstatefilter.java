
package Model.Distributerstatefilter;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Distributerstatefilter {

    @SerializedName("data")
    private List<Distributerstatedetails> mData;
    @SerializedName("result")
    private String mResult;

    public List<Distributerstatedetails> getData() {
        return mData;
    }

    public void setData(List<Distributerstatedetails> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
