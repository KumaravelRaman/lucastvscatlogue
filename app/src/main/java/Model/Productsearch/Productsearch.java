
package Model.Productsearch;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Productsearch {

    @SerializedName("data")
    private List<Productfilterlist> mData;
    @SerializedName("result")
    private String mResult;

    public List<Productfilterlist> getData() {
        return mData;
    }

    public void setData(List<Productfilterlist> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
