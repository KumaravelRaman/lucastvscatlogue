package Model.ServiceData;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class ServiceList {

    @SerializedName("Region")
    @Expose
    private String region;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("Dealerid")
    @Expose
    private String dealerid;
    @SerializedName("Dealername")
    @Expose
    private String dealername;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("Contactperson")
    @Expose
    private String contactperson;
    @SerializedName("Mobileno")
    @Expose
    private String mobileno;
    @SerializedName("Landlineno")
    @Expose
    private String landlineno;
    @SerializedName("Email_id")
    @Expose
    private String emailId;
    @SerializedName("Reportingtvsgroup")
    @Expose
    private String reportingtvsgroup;
    @SerializedName("Depotname")
    @Expose
    private String depotname;
    @SerializedName("Depocode")
    @Expose
    private String depocode;
    @SerializedName("sno")
    @Expose
    private Integer sno;
    @SerializedName("City")
    @Expose
    private String city;

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDealerid() {
        return dealerid;
    }

    public void setDealerid(String dealerid) {
        this.dealerid = dealerid;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactperson() {
        return contactperson;
    }

    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getLandlineno() {
        return landlineno;
    }

    public void setLandlineno(String landlineno) {
        this.landlineno = landlineno;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getReportingtvsgroup() {
        return reportingtvsgroup;
    }

    public void setReportingtvsgroup(String reportingtvsgroup) {
        this.reportingtvsgroup = reportingtvsgroup;
    }

    public String getDepotname() {
        return depotname;
    }

    public void setDepotname(String depotname) {
        this.depotname = depotname;
    }

    public String getDepocode() {
        return depocode;
    }

    public void setDepocode(String depocode) {
        this.depocode = depocode;
    }

    public Integer getSno() {
        return sno;
    }

    public void setSno(Integer sno) {
        this.sno = sno;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
