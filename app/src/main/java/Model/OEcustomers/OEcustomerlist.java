
package Model.OEcustomers;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OEcustomerlist {

    @SerializedName("Oem_Customer")
    private String mOemCustomer;
    @SerializedName("Oem_Id")
    private Long mOemId;
    @SerializedName("Oem_image")
    private String mOemImage;
    @SerializedName("Oem_priority")
    private Long mOemPriority;

    public String getOemCustomer() {
        return mOemCustomer;
    }

    public void setOemCustomer(String OemCustomer) {
        mOemCustomer = OemCustomer;
    }

    public Long getOemId() {
        return mOemId;
    }

    public void setOemId(Long OemId) {
        mOemId = OemId;
    }

    public String getOemImage() {
        return mOemImage;
    }

    public void setOemImage(String OemImage) {
        mOemImage = OemImage;
    }

    public Long getOemPriority() {
        return mOemPriority;
    }

    public void setOemPriority(Long OemPriority) {
        mOemPriority = OemPriority;
    }

}
