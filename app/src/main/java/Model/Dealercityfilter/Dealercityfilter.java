
package Model.Dealercityfilter;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Dealercityfilter {

    @SerializedName("data")
    private List<Dealercitydetails> mData;
    @SerializedName("result")
    private String mResult;

    public List<Dealercitydetails> getData() {
        return mData;
    }

    public void setData(List<Dealercitydetails> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
