
package Model.Partnosearch;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Partnosearch {

    @SerializedName("data")
    private List<partnolist> mData;
    @SerializedName("result")
    private String mResult;

    public List<partnolist> getData() {
        return mData;
    }

    public void setData(List<partnolist> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
