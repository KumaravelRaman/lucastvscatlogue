
package Model.Partnosearch;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class partnolist {

    @SerializedName("App_id")
    private String mAppId;
    @SerializedName("Appname")
    private String mAppname;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Engin_type")
    private String mEnginType;
    @SerializedName("expoledview")
    private String mExpoledview;
    @SerializedName("Full_Unit_No")
    private String mFullUnitNo;
    @SerializedName("Id")
    private String mId;
    @SerializedName("Mrp")
    private String mMrp;
    @SerializedName("OEname")
    private String mOEname;
    @SerializedName("Oem_id")
    private String mOemId;
    @SerializedName("Oem_partno")
    private String mOemPartno;
    @SerializedName("OldMrp")
    private String mOldMrp;
    @SerializedName("Part_No")
    private String mPartNo;
    @SerializedName("Part_Outputrng")
    private String mPartOutputrng;
    @SerializedName("Part_Volt")
    private String mPartVolt;
    @SerializedName("Plant")
    private String mPlant;
    @SerializedName("Pro_exp_iamge")
    private String mProExpIamge;
    @SerializedName("Pro_id")
    private String mProId;
    @SerializedName("Pro_image")
    private String mProImage;
    @SerializedName("Pro_model")
    private String mProModel;
    @SerializedName("pro_status")
    private String mProStatus;
    @SerializedName("Pro_supersed")
    private String mProSupersed;
    @SerializedName("Pro_type")
    private String mProType;
    @SerializedName("Productname")
    private String mProductname;
    @SerializedName("Unittype")
    private String mUnittype;
    @SerializedName("Uos")
    private String mUos;

    public String getAppId() {
        return mAppId;
    }

    public void setAppId(String AppId) {
        mAppId = AppId;
    }

    public String getAppname() {
        return mAppname;
    }

    public void setAppname(String Appname) {
        mAppname = Appname;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        mDescription = Description;
    }

    public String getEnginType() {
        return mEnginType;
    }

    public void setEnginType(String EnginType) {
        mEnginType = EnginType;
    }

    public String getExpoledview() {
        return mExpoledview;
    }

    public void setExpoledview(String expoledview) {
        mExpoledview = expoledview;
    }

    public String getFullUnitNo() {
        return mFullUnitNo;
    }

    public void setFullUnitNo(String FullUnitNo) {
        mFullUnitNo = FullUnitNo;
    }

    public String getId() {
        return mId;
    }

    public void setId(String Id) {
        mId = Id;
    }

    public String getMrp() {
        return mMrp;
    }

    public void setMrp(String Mrp) {
        mMrp = Mrp;
    }

    public String getOEname() {
        return mOEname;
    }

    public void setOEname(String OEname) {
        mOEname = OEname;
    }

    public String getOemId() {
        return mOemId;
    }

    public void setOemId(String OemId) {
        mOemId = OemId;
    }

    public String getOemPartno() {
        return mOemPartno;
    }

    public void setOemPartno(String OemPartno) {
        mOemPartno = OemPartno;
    }

    public String getOldMrp() {
        return mOldMrp;
    }

    public void setOldMrp(String OldMrp) {
        mOldMrp = OldMrp;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String PartNo) {
        mPartNo = PartNo;
    }

    public String getPartOutputrng() {
        return mPartOutputrng;
    }

    public void setPartOutputrng(String PartOutputrng) {
        mPartOutputrng = PartOutputrng;
    }

    public String getPartVolt() {
        return mPartVolt;
    }

    public void setPartVolt(String PartVolt) {
        mPartVolt = PartVolt;
    }

    public String getPlant() {
        return mPlant;
    }

    public void setPlant(String Plant) {
        mPlant = Plant;
    }

    public String getProExpIamge() {
        return mProExpIamge;
    }

    public void setProExpIamge(String ProExpIamge) {
        mProExpIamge = ProExpIamge;
    }

    public String getProId() {
        return mProId;
    }

    public void setProId(String ProId) {
        mProId = ProId;
    }

    public String getProImage() {
        return mProImage;
    }

    public void setProImage(String ProImage) {
        mProImage = ProImage;
    }

    public String getProModel() {
        return mProModel;
    }

    public void setProModel(String ProModel) {
        mProModel = ProModel;
    }

    public String getProStatus() {
        return mProStatus;
    }

    public void setProStatus(String proStatus) {
        mProStatus = proStatus;
    }

    public String getProSupersed() {
        return mProSupersed;
    }

    public void setProSupersed(String ProSupersed) {
        mProSupersed = ProSupersed;
    }

    public String getProType() {
        return mProType;
    }

    public void setProType(String ProType) {
        mProType = ProType;
    }

    public String getProductname() {
        return mProductname;
    }

    public void setProductname(String Productname) {
        mProductname = Productname;
    }

    public String getUnittype() {
        return mUnittype;
    }

    public void setUnittype(String Unittype) {
        mUnittype = Unittype;
    }

    public String getUos() {
        return mUos;
    }

    public void setUos(String Uos) {
        mUos = Uos;
    }

}
