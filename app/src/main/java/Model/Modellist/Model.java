
package Model.Modellist;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Model {

    @SerializedName("data")
    private List<AppmodelList> mData;
    @SerializedName("result")
    private String mResult;

    public List<AppmodelList> getData() {
        return mData;
    }

    public void setData(List<AppmodelList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
