package Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ltvscatalogue.R;

import java.util.List;

import Model.Distributor.DistributoDataItem;

public class DistirbutorDataAdapter extends RecyclerView.Adapter<DistirbutorDataAdapter.ViewHolder> {

    List<DistributoDataItem> statefilter;
    Context context;

    public DistirbutorDataAdapter(Context context, List<DistributoDataItem> statefilter) {
        this.context = context;
        this.statefilter = statefilter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.distributordataadapter, viewGroup, false);
        context = view.getContext();
        return new DistirbutorDataAdapter.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {

//        viewHolder.ed_company.setText(statefilter.get(i).getDealername());
        viewHolder.ed_name.setText(statefilter.get(i).getContactperson());
        viewHolder.ed_delercode.setText(statefilter.get(i).getCustomercode());
        viewHolder.ed_phone.setText(statefilter.get(i).getMobileno());
        viewHolder.ed_email.setText(statefilter.get(i).getEmailId());
        viewHolder.ed_address.setText(statefilter.get(i).getAddress() + statefilter.get(i).getAddress());

    }

    @Override
    public int getItemCount() {
        return statefilter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView ed_company, ed_name, ed_delercode, ed_phone, ed_email, ed_address;

        public ViewHolder(View view) {
            super(view);
            ed_company = (TextView) view.findViewById(R.id.shopname);
            ed_name = (TextView) view.findViewById(R.id.edt_name);

            ed_delercode = (TextView) view.findViewById(R.id.del_code);
            ed_phone = (TextView) view.findViewById(R.id.phone);
            ed_email = (TextView) view.findViewById(R.id.email);
            ed_address = (TextView) view.findViewById(R.id.del_address);
        }
    }



}
