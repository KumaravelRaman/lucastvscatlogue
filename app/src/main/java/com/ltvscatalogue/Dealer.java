package com.ltvscatalogue;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import APIInterface.CategoryAPI;
import Adapter.Dealeradapter;
import Adapter.Distributeradapter;
import Adapter.Partadpt;
import Adapter.ProductgridAdapter;
import Model.CityModel.CityModel;
import Model.Dealercity.Dealercitylist;
import Model.Dealercityfilter.Dealercityfilter;
import Model.Dealerstate.Dealerstatelist;
import Model.Dealetstatefilter.Dealerstatedetails;
import Model.Dealetstatefilter.Dealerstatefilter;
import Model.Distributerstatefilter.Distributerstatedetails;
import Model.Partnolist.Partnolist;
import Model.Partnosearch.Partnosearch;
import Model.Productsearch.Productsearch;
import Model.Rating.Ratinglist;
import Model.ServiceData.DataClass;
import Model.StateModel.DealerStateModel;
import RetroClient.RetroClient;
import alertbox.Alertbox;
import database.AppDatabase;
import network.NetworkConnection;
import persistence.DBHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static database.AppDatabase.getAppDatabase;

public class Dealer extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    List<String> statelist;
    List<String> citylist;
    Dealeradapter adapter;
    List<Dealerstatedetails> statefilter;
    List<Dealercityfilter> cityfilter;
    String State, City, getStateName, getCityName;
    TextView d_state, d_city;
    private Button btn_prev;
    private Button btn_next;
    private int pageCount;
    public int NUM_ITEMS_PAGE = 1;
    public int TOTAL_LIST_ITEMS = 0;
    private int increment = 0;
    public int val = 0;
    LinearLayout Pagination;
    NetworkConnection net;
    private AlertDialog alertDialog;
    private Alertbox box = new Alertbox(Dealer.this);
    private AppDatabase db;
    MaterialSpinner statespin, cityspin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer);

        DBHelper dbHelper = new DBHelper(Dealer.this);
        SQLiteDatabase sqLiteDatabase = dbHelper.readDataBase();

        db = getAppDatabase(Dealer.this);
        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        net = new NetworkConnection(Dealer.this);
        d_state = (TextView) findViewById(R.id.d_state);
        d_city = (TextView) findViewById(R.id.d_city);
        Pagination = (LinearLayout) findViewById(R.id.Pagination);
        statefilter = new ArrayList<>();
        cityfilter = new ArrayList<>();
        statelist = new ArrayList<String>();
        citylist = new ArrayList<String>();
        citylist.add("Select City");

        btn_prev = (Button) findViewById(R.id.prev);
        btn_next = (Button) findViewById(R.id.next);
        btn_prev.setVisibility(View.GONE);


        btn_next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                increment++;
                CheckEnable();
                getcityfilter();
            }
        });
        btn_prev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                increment--;
                CheckEnable();
                getcityfilter();
            }
        });
        alertDialog = new android.app.AlertDialog.Builder(
                Dealer.this).create();

        LayoutInflater inflater = (Dealer.this).getLayoutInflater();
        final View dialog = inflater.inflate(R.layout.dealeralert, null);
        alertDialog.setView(dialog);
        alertDialog.setCancelable(false);
        statespin = (MaterialSpinner) dialog.findViewById(R.id.statespin);
        cityspin = (MaterialSpinner) dialog.findViewById(R.id.cityspin);
        statespin.setBackground(getResources().getDrawable(R.drawable.autotextback));
        cityspin.setBackground(getResources().getDrawable(R.drawable.autotextback));


        CategoryAPI service = RetroClient.getApiService();

        Call<DealerStateModel> call = service.getStateList();

        call.enqueue(new Callback<DealerStateModel>() {
            @Override
            public void onResponse(Call<DealerStateModel> call, Response<DealerStateModel> response) {
                //Dismiss Dialog
                if (response.body().getResult().equals("success")) {
                    statelist.add("Select State");
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        statelist.add(response.body().getData().get(i));
                    }
                    statespin.setItems(statelist);

                    statespin.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                            citylist.clear();
                            getStateName = statespin.getText().toString();
                            getCityDetails();
                            d_state.setText(getStateName);
                        }
                    });

                } else {
                    Toast.makeText(Dealer.this, "No Record Found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DealerStateModel> call, Throwable t) {
                Toast.makeText(Dealer.this, "No Record Found", Toast.LENGTH_SHORT).show();
            }
        });


        Button search = (Button) dialog.findViewById(R.id.search);
        Button cancel = (Button) dialog.findViewById(R.id.cancel);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                State = statespin.getText().toString().trim();
                City = cityspin.getText().toString().trim();
                if (State.equals("Select State")) {
                    Toast.makeText(getApplicationContext(), "Select State", Toast.LENGTH_LONG).show();
                } else if (City.equals("Select City")) {
                    Toast.makeText(getApplicationContext(), "Select City", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        final ProgressDialog progressDialog = new ProgressDialog(Dealer.this,
                                R.style.Progress);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setMessage("Loading...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        CategoryAPI service = RetroClient.getApiService();
                        Call<DataClass> call = service.datasearch(getStateName, getCityName);
                        call.enqueue(new Callback<DataClass>() {
                            @Override
                            public void onResponse(Call<DataClass> call, Response<DataClass> response) {
                                progressDialog.dismiss();
                                if (response.isSuccessful()) {
                                    adapter = new Dealeradapter(getApplicationContext(), response.body().getData());
                                    layoutManager = new LinearLayoutManager(getApplicationContext());
                                    recyclerView.setLayoutManager(layoutManager);
                                    recyclerView.setAdapter(adapter);
                                    alertDialog.dismiss();
                                } else {

                                    alertDialog.dismiss();
                                    box.showAlertboxwithback("No Record Found");
                                }


                            }

                            @Override
                            public void onFailure(Call<DataClass> call, Throwable t) {

                                box.showAlertboxwithback("Something went wrong . Please try again later .");
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Poor Network..", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                alertDialog.dismiss();

            }
        });
        alertDialog.show();
    }

    private void CheckEnable() {
        try {
            TOTAL_LIST_ITEMS = statefilter.size();
            val = TOTAL_LIST_ITEMS % NUM_ITEMS_PAGE;
            val = val == 0 ? 0 : 1;
            pageCount = TOTAL_LIST_ITEMS / NUM_ITEMS_PAGE + val;
            if (increment == pageCount - 1) {
                btn_next.setVisibility(View.GONE);
            } else if (increment == 0) {
                btn_prev.setVisibility(View.GONE);
            } else {
                btn_prev.setVisibility(View.VISIBLE);
                btn_next.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    public void getcityfilter() {
        statefilter = db.distributorsDAO().getDealerList(State, City);
        TOTAL_LIST_ITEMS = statefilter.size();
        if (TOTAL_LIST_ITEMS < 3) {
            Pagination.setVisibility(View.GONE);
        } else {
            Pagination.setVisibility(View.VISIBLE);
        }
        List<Dealerstatedetails> sort = new ArrayList<>();
        int start = increment * NUM_ITEMS_PAGE;
        for (int i = start; i < (start) + NUM_ITEMS_PAGE; i++) {
            if (i < TOTAL_LIST_ITEMS) {
                sort.add(statefilter.get(i));
            }
        }

        alertDialog.dismiss();


    }


    public void Home(View view) {

        startActivity(new Intent(this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    public void Back(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void getCityDetails() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(Dealer.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);

            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();

            Call<CityModel> call = service.citysearch(getStateName);
            call.enqueue(new Callback<CityModel>() {
                @Override
                public void onResponse(Call<CityModel> call, Response<CityModel> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("success")) {

                        for (int i = 0; i < response.body().getData().size(); i++) {
                            citylist.add(response.body().getData().get(i));
                        }
                        cityspin.setItems(citylist);
                        getCityName = cityspin.getText().toString();
                        d_city.setText(getCityName);
                        cityspin.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                            }
                        });

                        cityspin.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                                getCityName = cityspin.getText().toString();

                            }
                        });
                    }

                }

                @Override
                public void onFailure(Call<CityModel> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Poor Network..", Toast.LENGTH_LONG).show();
        }
    }

}
