package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import javax.sql.StatementEvent;

import Model.Dealetstatefilter.Dealerstatedetails;
import Model.Distributercityfilter.Distributorcitydetails;
import Model.Distributerstatefilter.Distributerstatedetails;
import Model.Distributerstatefilter.Distributerstatefilter;


@Dao
public interface DistributorsDAO {



    // Distributors
    @Query("SELECT distinct state FROM distributors")
    List<String> getStateList();

    @Query("SELECT distinct city FROM distributors where state =:state")
    List<String> getCityList(String state);
    

    
    @Query("SELECT distinct * FROM distributors where state =:state and city =:city")
    List<Distributerstatedetails> getDistributorsList(String state, String city);



    // Dealers
    @Query("SELECT distinct state FROM servicedealer")
    List<String> getDealerStateList();

    @Query("SELECT distinct city FROM servicedealer where state =:state")
    List<String>    getDealerCityList(String state);

    @Query("SELECT distinct * FROM servicedealer where state =:state and city =:city")
    List<Dealerstatedetails> getDealerList(String state, String city);
}
